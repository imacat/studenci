package pl.edu.pk.studenci;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Wojciech Kocik
 * @since 31.01.2017
 */

public class AuthTokenStore {

    private Context context;
    private SharedPreferences sharedPreferences;

    public final static String tokenSharedPreferencesKey = "AUTH_TOKEN";

    public AuthTokenStore(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("pl.edu.pk.studenci", Context.MODE_PRIVATE);
    }

    public String get(){
        return sharedPreferences.getString(tokenSharedPreferencesKey, null);
    }

    public void save(String token){
        sharedPreferences
                .edit()
                .putString(tokenSharedPreferencesKey, token)
                .apply();
    }

    public void purge() {
        sharedPreferences
                .edit()
                .putString(tokenSharedPreferencesKey, null)
                .apply();
    }
}
