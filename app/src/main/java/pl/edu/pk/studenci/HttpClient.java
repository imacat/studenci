package pl.edu.pk.studenci;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.edu.pk.studenci.dto.LoginRequest;
import pl.edu.pk.studenci.dto.TokenResponse;
import pl.edu.pk.studenci.dto.rest.group.GroupItemResponse;
import pl.edu.pk.studenci.dto.rest.student.StudentItemResponse;
import pl.edu.pk.studenci.exception.BadHttpRequest;
import pl.edu.pk.studenci.exception.ServerError;
import pl.edu.pk.studenci.exception.WrongUserAuthenticationData;

/**
 * @author Wojciech Kocik
 * @since 30.01.2017
 */

public class HttpClient {

    private OkHttpClient httpClient;
    private Gson gson = new Gson();
    private Context context;
    private AuthTokenStore authTokenStore;

    public final static int HTTP_OK = 200;

    public final static String apiTokenHeaderName = "api_token";
    public final static String loginRequestPath = "auth";
    public final static String groupsRequestPath = "api/group";
    public final static String studentsInGroupRequestPath = "api/group/%s/students";
    public final static String studentsRequestPath = "api/student";
    public final static String groupByIdRequestPath = "api/group/%s";

    public HttpClient(Context context) {
        this.httpClient = new OkHttpClient();
        this.context = context;
        this.authTokenStore = new AuthTokenStore(context);
    }

    public TokenResponse login(LoginRequest loginRequest) throws WrongUserAuthenticationData, ServerError, BadHttpRequest {
        RequestBody formBody = new FormBody.Builder()
                .add("username", loginRequest.getLogin())
                .add("password", loginRequest.getPassword())
                .build();

        Request request = new Request.Builder()
                .url(UrlFactory.getUrl(loginRequestPath))
                .method("POST", formBody)
                .build();

        Response response = null;
        try {
            response = httpClient.newCall(request).execute();

            if (response.code() == HTTP_OK) {
                return gson.fromJson(response.body().string(), TokenResponse.class);
            } else {
                wrongRequestBehaviour(response.code());
                throw new ServerError();
            }

        } catch (IOException e) {
            Toast.makeText(context, R.string.server_communication_error, Toast.LENGTH_LONG).show();
            throw new ServerError();
        }
    }

    private void wrongRequestBehaviour(int code) throws WrongUserAuthenticationData, BadHttpRequest, ServerError {
        if (code == 403) {
            throw new WrongUserAuthenticationData();
        } else if (code == 400) {
            Toast.makeText(context, R.string.bad_server_request, Toast.LENGTH_LONG).show();
            throw new BadHttpRequest();
        }
    }

    public List<GroupItemResponse> getGroups() throws ServerError, BadHttpRequest, WrongUserAuthenticationData {
        Request request = new Request.Builder()
                .url(UrlFactory.getUrl(groupsRequestPath))
                .addHeader(apiTokenHeaderName, authTokenStore.get())
                .build();

        Response response = null;

        try {
            response = httpClient.newCall(request).execute();

            if (response.code() == HTTP_OK) {
                Type returnTypeObject = new TypeToken<ArrayList<GroupItemResponse>>() {
                }.getType();
                return gson.fromJson(response.body().string(), returnTypeObject);
            } else {
                wrongRequestBehaviour(response.code());
                throw new ServerError();
            }

        } catch (IOException e) {
            Toast.makeText(context, R.string.server_communication_error, Toast.LENGTH_LONG).show();
            throw new ServerError();
        }
    }

    public List<StudentItemResponse> getStudentsInGroupId(String groupId) throws ServerError, BadHttpRequest, WrongUserAuthenticationData {
        Request request = new Request.Builder()
                .url(UrlFactory.getUrl(String.format(studentsInGroupRequestPath, groupId)))
                .addHeader(apiTokenHeaderName, authTokenStore.get())
                .build();

        Response response = null;

        try {
            response = httpClient.newCall(request).execute();

            if (response.code() == HTTP_OK) {
                Type returnTypeObject = new TypeToken<ArrayList<StudentItemResponse>>() {
                }.getType();
                return gson.fromJson(response.body().string(), returnTypeObject);
            } else {
                wrongRequestBehaviour(response.code());
                throw new ServerError();
            }
        } catch (IOException e) {
            Toast.makeText(context, R.string.server_communication_error, Toast.LENGTH_LONG).show();
            throw new ServerError();
        }
    }

    public List<StudentItemResponse> getStudents() throws ServerError, BadHttpRequest, WrongUserAuthenticationData {
        Request request = new Request.Builder()
                .url(UrlFactory.getUrl(studentsRequestPath))
                .addHeader(apiTokenHeaderName, authTokenStore.get())
                .build();

        Response response = null;

        try {
            response = httpClient.newCall(request).execute();

            if (response.code() == HTTP_OK) {
                Type returnTypeObject = new TypeToken<ArrayList<StudentItemResponse>>() {
                }.getType();
                return gson.fromJson(response.body().string(), returnTypeObject);
            } else {
                wrongRequestBehaviour(response.code());
                throw new ServerError();
            }
        } catch (IOException e) {
            Toast.makeText(context, R.string.server_communication_error, Toast.LENGTH_LONG).show();
            throw new ServerError();
        }
    }

    public GroupItemResponse getGroupById(String groupId) throws ServerError, BadHttpRequest, WrongUserAuthenticationData {
        Request request = new Request.Builder()
                .url(UrlFactory.getUrl(String.format(groupByIdRequestPath, groupId)))
                .addHeader(apiTokenHeaderName, authTokenStore.get())
                .build();

        Response response = null;
        try{
            response = httpClient.newCall(request).execute();
            if (response.code() == HTTP_OK) {
                return gson.fromJson(response.body().string(), GroupItemResponse.class);
            } else {
                wrongRequestBehaviour(response.code());
                throw new ServerError();
            }

        } catch (IOException e) {
            Toast.makeText(context, R.string.server_communication_error, Toast.LENGTH_LONG).show();
            throw new ServerError();
        }
    }
}
