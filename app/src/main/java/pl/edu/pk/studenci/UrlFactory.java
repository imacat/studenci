package pl.edu.pk.studenci;

/**
 * @author Wojciech Kocik
 * @since 30.01.2017
 */

public class UrlFactory {
    public static String getUrl(String path){
        return Config.serverUrl + ":" + Config.serverPort + "/" + path;
    }
}
