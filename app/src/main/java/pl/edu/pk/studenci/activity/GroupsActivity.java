package pl.edu.pk.studenci.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pk.studenci.AuthTokenStore;
import pl.edu.pk.studenci.HttpClient;
import pl.edu.pk.studenci.R;
import pl.edu.pk.studenci.adapter.GroupListAdapter;
import pl.edu.pk.studenci.adapter.RecyclerItemClickListener;
import pl.edu.pk.studenci.adapter.SimpleDividerItemDecoration;
import pl.edu.pk.studenci.adapter.StudentListAdapter;
import pl.edu.pk.studenci.dto.GroupListItem;
import pl.edu.pk.studenci.dto.StudentListItem;
import pl.edu.pk.studenci.dto.rest.group.GroupItemResponse;
import pl.edu.pk.studenci.exception.BadHttpRequest;
import pl.edu.pk.studenci.exception.ServerError;
import pl.edu.pk.studenci.exception.WrongUserAuthenticationData;

public class GroupsActivity extends AppCompatActivity {

    private RecyclerView mGroupsRecyclerView;
    private HttpClient mHttpClient;
    private AuthTokenStore mAuthTokenStore;
    private ProgressBar loadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHttpClient = new HttpClient(this);
        mAuthTokenStore = new AuthTokenStore(this);

        mGroupsRecyclerView = (RecyclerView) findViewById(R.id.groups_recycler_view);
        mGroupsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mGroupsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        loadingProgressBar = (ProgressBar) findViewById(R.id.groups_progress_bar);

        mGroupsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                GroupListAdapter groupListAdapter = (GroupListAdapter) mGroupsRecyclerView.getAdapter();
                GroupListItem groupListItem = groupListAdapter.getGroupListItems().get(position);
                String groupId = groupListItem.getId();
                Intent studentIntentActivity = new Intent(getApplicationContext(), StudentsActivity.class);
                studentIntentActivity.putExtra("groupId", groupId);
                studentIntentActivity.putExtra("groupName", groupListItem.getName());
                startActivity(studentIntentActivity);
            }
        }));

        new GetGroupsTask().execute();
    }

    private void goToLoginActivity() {
        mAuthTokenStore.purge();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public class GetGroupsTask extends AsyncTask<Void, Void, List<GroupItemResponse>> {

        @Override
        protected void onPreExecute() {
            loadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<GroupItemResponse> doInBackground(Void... params) {
            List<GroupItemResponse> groupItemsResponse = null;
            try {
                groupItemsResponse = mHttpClient.getGroups();
            } catch (ServerError | BadHttpRequest serverError) {
                Toast.makeText(getApplicationContext(), R.string.server_request_failed, Toast.LENGTH_LONG).show();
            } catch (WrongUserAuthenticationData wrongUserAuthenticationData) {
                Toast.makeText(getApplicationContext(), R.string.authentication_failed, Toast.LENGTH_LONG).show();
                goToLoginActivity();
            }
            return groupItemsResponse;
        }

        @Override
        protected void onPostExecute(List<GroupItemResponse> groupItemResponses) {
            List<GroupListItem> groupListItems = new ArrayList<>();
            for (GroupItemResponse groupItemResponse : groupItemResponses) {
                GroupListItem groupListItem = new GroupListItem();
                groupListItem.setStudentCount(
                        Integer.parseInt(groupItemResponse.getCount().getStudents())
                );
                groupListItem.setName(groupItemResponse.getName());
                groupListItem.setId(groupItemResponse.getId());
                groupListItem.setYear(groupItemResponse.getYear());
                groupListItems.add(groupListItem);
            }

            GroupListAdapter groupListAdapter = new GroupListAdapter();

            groupListAdapter.setGroupListItems(groupListItems);

            mGroupsRecyclerView.setAdapter(groupListAdapter);
            loadingProgressBar.setVisibility(View.GONE);
            groupListAdapter.notifyDataSetChanged();
        }
    }

}
