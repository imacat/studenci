package pl.edu.pk.studenci.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import pl.edu.pk.studenci.AuthTokenStore;
import pl.edu.pk.studenci.R;

public class MainActivity extends AppCompatActivity {

    private AuthTokenStore mAuthTokenStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuthTokenStore = new AuthTokenStore(this);

        Button logOutButton = (Button) findViewById(R.id.log_out_button);
        Button groupsButton = (Button) findViewById(R.id.groups_button);
        Button studentsButton = (Button) findViewById(R.id.students_button);


        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuthTokenStore.purge();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        groupsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GroupsActivity.class);
                startActivity(intent);
            }
        });

        studentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StudentsActivity.class);
                startActivity(intent);
            }
        });
    }

}
