package pl.edu.pk.studenci.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pk.studenci.AuthTokenStore;
import pl.edu.pk.studenci.HttpClient;
import pl.edu.pk.studenci.R;
import pl.edu.pk.studenci.adapter.SimpleDividerItemDecoration;
import pl.edu.pk.studenci.adapter.StudentListAdapter;
import pl.edu.pk.studenci.dto.StudentListItem;
import pl.edu.pk.studenci.dto.rest.group.GroupItemResponse;
import pl.edu.pk.studenci.dto.rest.student.StudentItemResponse;
import pl.edu.pk.studenci.dto.rest.student.StudentItemResponseWithGroupNameAggregation;
import pl.edu.pk.studenci.exception.BadHttpRequest;
import pl.edu.pk.studenci.exception.ServerError;
import pl.edu.pk.studenci.exception.WrongUserAuthenticationData;

public class StudentsActivity extends AppCompatActivity {

    private RecyclerView mStudentsRecyclerView;
    private AuthTokenStore mAuthTokenStore;
    private HttpClient mHttpClient;
    private ProgressBar loadingProgressBar;

    private String groupId, groupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHttpClient = new HttpClient(this);
        mAuthTokenStore = new AuthTokenStore(this);

        groupId = getIntent().getStringExtra("groupId");
        groupName = getIntent().getStringExtra("groupName");

        mStudentsRecyclerView = (RecyclerView) findViewById(R.id.students_recycler_view);
        mStudentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mStudentsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        loadingProgressBar = (ProgressBar) findViewById(R.id.students_progress_bar);

        GetStudentsTask getStudentsTask = new GetStudentsTask();
        getStudentsTask.execute();
    }

    private void goToLoginActivity() {
        mAuthTokenStore.purge();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    class GetStudentsTask extends AsyncTask<Void, Void, List<StudentItemResponseWithGroupNameAggregation>> {

        @Override
        protected void onPreExecute() {
            loadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<StudentItemResponseWithGroupNameAggregation> doInBackground(Void... params) {

            List<StudentItemResponse> studentItemsResponse;
            List<StudentItemResponseWithGroupNameAggregation> studentItemsResponseAggregate = new ArrayList<>();
            try {
                if (groupId == null || groupId.isEmpty()) {
                    studentItemsResponse = mHttpClient.getStudents();
                } else {
                    studentItemsResponse = mHttpClient.getStudentsInGroupId(groupId);
                }

                for (StudentItemResponse studentItemResponse : studentItemsResponse) {
                    StudentItemResponseWithGroupNameAggregation studentItemResponseWithGroupNameAggregation =
                            new StudentItemResponseWithGroupNameAggregation(studentItemResponse);
                    if (groupName == null || groupName.isEmpty()) {
                        GroupItemResponse groupItemResponse = mHttpClient.getGroupById(studentItemResponse.getGroup_id());
                        studentItemResponseWithGroupNameAggregation.setGroupName(groupItemResponse.getName());
                    } else {
                        studentItemResponseWithGroupNameAggregation.setGroupName(groupName);
                    }
                    studentItemsResponseAggregate.add(studentItemResponseWithGroupNameAggregation);
                }


            } catch (ServerError | BadHttpRequest serverError) {
                Toast.makeText(getApplicationContext(), R.string.server_request_failed, Toast.LENGTH_LONG).show();
            } catch (WrongUserAuthenticationData wrongUserAuthenticationData) {
                Toast.makeText(getApplicationContext(), R.string.authentication_failed, Toast.LENGTH_LONG).show();
                goToLoginActivity();
            }
            return studentItemsResponseAggregate;
        }

        @Override
        protected void onPostExecute(List<StudentItemResponseWithGroupNameAggregation> studentItemResponses) {
            List<StudentListItem> studentListItems = new ArrayList<>();
            for (StudentItemResponseWithGroupNameAggregation studentItemResponse : studentItemResponses) {
                StudentListItem studentListItem = new StudentListItem();
                studentListItem.setId(studentItemResponse.getId());
                studentListItem.setFirstName(studentItemResponse.getFirstname());
                studentListItem.setSecondName(studentItemResponse.getSecondname());
                studentListItem.setLastName(studentItemResponse.getLastname());
                studentListItem.setGroupName(studentItemResponse.getGroupName());
                studentListItems.add(studentListItem);
            }

            StudentListAdapter studentListAdapter = new StudentListAdapter();
            studentListAdapter.setStudentListItems(studentListItems);
            mStudentsRecyclerView.setAdapter(studentListAdapter);
            loadingProgressBar.setVisibility(View.GONE);
            studentListAdapter.notifyDataSetChanged();
        }
    }
}
