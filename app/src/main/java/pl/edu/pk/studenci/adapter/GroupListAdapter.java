package pl.edu.pk.studenci.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.edu.pk.studenci.R;
import pl.edu.pk.studenci.activity.StudentsActivity;
import pl.edu.pk.studenci.dto.GroupListItem;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.ViewHolder> {

    private List<GroupListItem> groupListItems;

    public List<GroupListItem> getGroupListItems() {
        return groupListItems;
    }

    public void setGroupListItems(List<GroupListItem> groupListItems) {
        this.groupListItems = groupListItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GroupListItem groupListItem = groupListItems.get(position);

        holder.name.setText(groupListItem.getName());
        holder.year.setText(groupListItem.getYear());
        holder.studentCount.setText(String.format("Studenci: %s", String.valueOf(groupListItem.getCount())));
    }

    @Override
    public int getItemCount() {
        return groupListItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView year;
        TextView studentCount;

        ViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.name_group_item);
            this.studentCount = (TextView) itemView.findViewById(R.id.student_count_group_item);
            this.year = (TextView) itemView.findViewById(R.id.year_group_item);
        }
    }
}
