package pl.edu.pk.studenci.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.edu.pk.studenci.R;
import pl.edu.pk.studenci.dto.StudentListItem;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.ViewHolder> {

    public List<StudentListItem> getStudentListItems() {
        return studentListItems;
    }

    private List<StudentListItem> studentListItems;

    public void setStudentListItems(List<StudentListItem> studentListItems) {
        this.studentListItems = studentListItems;
    }

    @Override
    public StudentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StudentListItem studentListItem = studentListItems.get(position);
        holder.firstNameAndSecondName.setText(String.format("%s %s", studentListItem.getFirstName(), studentListItem.getSecondName()));
        holder.lastName.setText(studentListItem.getLastName());
        holder.groupName.setText(studentListItem.getGroupName());
    }

    @Override
    public int getItemCount() {
        return studentListItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView firstNameAndSecondName;
        TextView lastName;
        TextView groupName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.firstNameAndSecondName = (TextView) itemView.findViewById(R.id.firstname_and_secondname_student_item);
            this.lastName = (TextView) itemView.findViewById(R.id.lastname_student_item);
            this.groupName = (TextView) itemView.findViewById(R.id.student_group_student_item);
        }
    }
}
