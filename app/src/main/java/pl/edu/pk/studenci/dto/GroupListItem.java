package pl.edu.pk.studenci.dto;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class GroupListItem {
    private String id;
    private int count;
    private String name;
    private String year;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setStudentCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
