package pl.edu.pk.studenci.dto;

/**
 * @author Wojciech Kocik
 * @since 31.01.2017
 */

public class TokenResponse {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
