package pl.edu.pk.studenci.dto.rest.group;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class GroupItemResponse {
    private String id;
    private StudentCount count;
    private String name;
    private String year;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StudentCount getCount() {
        return count;
    }

    public void setCount(StudentCount count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
