package pl.edu.pk.studenci.dto.rest.group;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class StudentCount {
    private String students;

    public String getStudents() {
        return students;
    }

    public void setStudents(String students) {
        this.students = students;
    }
}
