package pl.edu.pk.studenci.dto.rest.student;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */
public class StudentItemResponse {
    private String group_id;
    private String id;
    private String lastname;
    private String secondname;
    private String firstname;

    public StudentItemResponse(StudentItemResponse studentItemResponse){
        this.group_id = studentItemResponse.getGroup_id();
        this.id = studentItemResponse.getId();
        this.lastname = studentItemResponse.getLastname();
        this.secondname = studentItemResponse.getSecondname();
        this.firstname = studentItemResponse.getFirstname();
    }

    public StudentItemResponse() {
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
}
