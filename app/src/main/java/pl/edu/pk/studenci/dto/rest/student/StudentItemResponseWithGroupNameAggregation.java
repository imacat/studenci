package pl.edu.pk.studenci.dto.rest.student;

/**
 * @author Wojciech Kocik
 * @since 04.02.2017
 */

public class StudentItemResponseWithGroupNameAggregation extends StudentItemResponse {
    private String groupName;

    public StudentItemResponseWithGroupNameAggregation() {
    }

    public StudentItemResponseWithGroupNameAggregation(StudentItemResponse studentItemResponse) {
        super(studentItemResponse);
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
