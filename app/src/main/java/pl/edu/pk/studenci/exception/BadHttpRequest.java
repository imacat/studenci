package pl.edu.pk.studenci.exception;

/**
 * @author Wojciech Kocik
 * @since 31.01.2017
 */
public class BadHttpRequest extends Exception {
    public BadHttpRequest() {
    }

    public BadHttpRequest(String detailMessage) {
        super(detailMessage);
    }

    public BadHttpRequest(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public BadHttpRequest(Throwable throwable) {
        super(throwable);
    }
}
