package pl.edu.pk.studenci.exception;

/**
 * @author Wojciech Kocik
 * @since 31.01.2017
 */
public class ServerError extends Exception {
    public ServerError() {
    }

    public ServerError(String detailMessage) {
        super(detailMessage);
    }

    public ServerError(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ServerError(Throwable throwable) {
        super(throwable);
    }
}
