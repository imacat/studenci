package pl.edu.pk.studenci.exception;

/**
 * @author Wojciech Kocik
 * @since 30.01.2017
 */
public class WrongUserAuthenticationData extends Exception {
    public WrongUserAuthenticationData() {
    }

    public WrongUserAuthenticationData(String detailMessage) {
        super(detailMessage);
    }

    public WrongUserAuthenticationData(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WrongUserAuthenticationData(Throwable throwable) {
        super(throwable);
    }
}
